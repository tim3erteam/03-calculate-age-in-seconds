public class Calculator {

    public void timeCalculation(int year, int month, int day, int hour, int minute, int second) {
        System.out.println("Time in seconds : " + calculationTimeToSeconds(year, month, day, hour, minute, second));
        System.out.println("Time in minute : " + calculationTimeToSMinute(year, month, day, hour, minute, second));
        System.out.println("Time in hour : " + calculationTimeToHours(year, month, day, hour, minute, second));
        System.out.println("Time in day : " + calculationTimeToDay(year, month, day, hour, minute, second));
        System.out.println("Time in month : " + calculationTimeToMonth(year, month, day, hour, minute, second));
        System.out.println("Time in yeat: " + calculationTimeToYear(year, month, day, hour, minute, second));
    }


    private long calculationTimeToSeconds(int year, int month, int day, int hour, int minute,
                                          int second) {
        return (second + minute * 60 + hour * 60 * 60 + day * 24 * 60 * 60 + month * 30 * 24 * 60 * 60 + year * 12 * 30 * 24 * 60 * 60);
    }

    private double  calculationTimeToSMinute(int year, int month, int day, int hour, int minute,
                                          int second) {
        return (second + minute * 60 + hour * 60 * 60 + day * 24 * 60 * 60 + month * 30 * 24 * 60 * 60 + year * 12 * 30 * 24 * 60 * 60)/(60);
    }

    private double calculationTimeToHours(int year, int month, int day, int hour, int minute,
                                        int second) {
        return (second + minute * 60 + hour * 60 * 60 + day * 24 * 60 * 60 + month * 30 * 24 * 60 * 60 + year * 12 * 30 * 24 * 60 * 60)/(60*60);
    }

    private long calculationTimeToDay(int year, int month, int day, int hour, int minute,
                                      int second) {
        return (second + minute * 60 + hour * 60 * 60 + day * 24 * 60 * 60 + month * 30 * 24 * 60 * 60 + year * 12 * 30 * 24 * 60 * 60)/(60*60*24);
    }

    private double calculationTimeToMonth(int year, int month, int day, int hour, int minute,
                                        int second) {
        return (second + minute * 60 + hour * 60 * 60 + day * 24 * 60 * 60 + month * 30 * 24 * 60 * 60 + year * 12 * 30 * 24 * 60 * 6)/(60*60*24*30);

    }

    private double calculationTimeToYear(int year, int month, int day, int hour, int minute,
                                       int second) {
        return (second + minute * 60 + hour * 60 * 60 + day * 24 * 60 * 60 + month * 30 * 24 * 60 * 60 + year * 12 * 30 * 24 * 60 * 60)/(60*60*24*30*12);
    }

}
